import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import LoginScreen from './Login';

ReactDOM.render(<LoginScreen />, document.getElementById('root'));
