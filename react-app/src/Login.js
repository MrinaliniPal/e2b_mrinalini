import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import axios from 'axios';

var apiBaseUrl = "http://localhost:5000/api/";


class Login extends Component {
constructor(props){
  super(props);
  this.state={
  email:'',
  password:''
  }
 }


render() {
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <AppBar
             title="Login"
           />
           <TextField
             hintText="Enter your Email"
             floatingLabelText="Username"
             onChange = {(event,newValue) => this.setState({email:newValue})}
             />
           <br/>
             <TextField
               type="password"
               hintText="Enter your Password"
               floatingLabelText="Password"
               onChange = {(event,newValue) => this.setState({password:newValue})}
               />
             <br/>
             <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}/>
         </div>
         </MuiThemeProvider>
      </div>
    );
  }

  handleClick(event){
    var self = this;
    var payload={
      "email":this.state.email,
      "password":this.state.password,
    }
    axios.post(apiBaseUrl+'login', payload)
   .then(function (response) {
     //console.log(response);
     if(response.data.code == 200){
       console.log(response.data.message);
     }
     else if(response.data.code == 208){
       console.log(response.data.message);
     }
     else{
       console.log(response.data.message);
     }
   })
   .catch(function (error) {
     console.log(error);
   });
  }

}


const style = {
 margin: 15,
};

export default Login;
